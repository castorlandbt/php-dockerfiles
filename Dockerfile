FROM php:7.3-apache

#Install necessary packages
RUN apt-get update \
    && apt-get install -y git libpng-dev \
    wget \
    rsync \
    nano \
    cron \
    zip \
    libzip-dev libicu-dev g++ \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libxpm-dev \
    libfreetype6-dev \
    libmagickwand-dev \
    imagemagick libmagickcore-dev \
    libmemcached-dev \
    && pecl install imagick \
    && pecl install memcached
RUN docker-php-ext-configure intl
RUN docker-php-ext-configure zip --with-libzip
RUN docker-php-ext-configure gd --with-gd --with-webp-dir --with-jpeg-dir \
    --with-png-dir --with-zlib-dir --with-xpm-dir --with-freetype-dir \
    --enable-gd-native-ttf
RUN docker-php-ext-install pdo pdo_mysql gd mysqli intl zip
RUN docker-php-ext-enable imagick \
    memcached \
    opcache

# Install APCu and APC backward compatibility
RUN pecl install apcu \
    && pecl install apcu_bc-1.0.3 \
    && docker-php-ext-enable apcu --ini-name 10-docker-php-ext-apcu.ini \
    && docker-php-ext-enable apc --ini-name 20-docker-php-ext-apc.ini

# Install PHPUnit
RUN wget https://phar.phpunit.de/phpunit.phar -O /usr/local/bin/phpunit \
    && chmod +x /usr/local/bin/phpunit

# Clean repository
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Set up Apache
ENV APACHE_DOCUMENT_ROOT=/var/www/web
RUN a2enmod rewrite \
    && sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf \
    && sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

#Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=. --filename=composer \
    && mv composer /usr/local/bin/

# Cron
# ADD .cron /etc/cron.d/cron
# RUN chmod 0644 /etc/cron.d/cron \
#    && touch /var/log/cron.log \
#    && service cron start

EXPOSE 80
EXPOSE 443