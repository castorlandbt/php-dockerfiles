# PHP7.3-apache-packages

apt-get install -y git libpng-dev \
    wget \
    nano \
    cron \
    zip \
    libzip-dev libicu-dev g++ \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libxpm-dev \
    libfreetype6-dev \
    libmagickwand-dev \
    imagemagick libmagickcore-dev \
    libmemcached-dev \
    && pecl install imagick \
    && pecl install memcached

++

phpunit
composer